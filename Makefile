.PHONY: all install-jena
all:
	echo 'try "make jena.sif" or "make install-jena"'  
jena.sif:
	singularity -v build jena.sif docker://registry.gitlab.com/wai-te-ata-press/jena
	chmod -w jena.sif
install-jena:
	git clone https://github.com/apache/jena.git
	cd jena && git checkout tags/jena-3.14.0
	cd jena && mvn clean install --log-file ../install-jena.log
TextQuery:
	cp jena/jena-text/testing/TextQuery .
jena-text:
	cp -r jena/jena-text/ .
TDB2:
	# singularity shell jena.sif
	java -cp $(FUSEKI_HOME)/fuseki-server.jar tdb2.tdbloader --loc=TDB2 TextQuery/data1.ttl 
index:
	# singularity shell jena.sif
	java -cp $(FUSEKI_HOME)/fuseki-server.jar jena.textindexer --desc=TextQuery/text-config.ttl

