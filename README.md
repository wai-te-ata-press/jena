# jena

Trying to get our SPARQL text query going

## singularity (mahuika)
```
git clone https://gitlab.com/wai-te-ata-press/jena.git
cd jena
module load Singularity
make jena.sif
singularity shell jena.sif
> make install-jena
```

## podman
```
podman login registry.gitlab.com
podman pull registry.gitlab.com/wai-te-ata-press/jena
podman run -it --rm --ulimit nofile=4096:4096 registry.gitlab.com/wai-te-ata-press/jena /bin/bash
> git clone https://gitlab.com/wai-te-ata-press/jena.git
> cd jena
> make install-jena
```
## pointing fuseki server at textDataset

### run these commands in a sparqler machine that has jena project cloned in it (interim step)

```
java -cp $FUSEKI_HOME/fuseki-server.jar tdb2.tdbloader --loc=TDB2 TextQuery/data1.ttl
java -cp $FUSEKI_HOME/fuseki-server.jar jena.textindexer --desc=TextQuery/text-config.ttl
java -jar $FUSEKI_HOME/fuseki-server.jar --conf=TextQuery/text-config.ttl --timeout=10000
```